# hsmfui [![Build Status](https://travis-ci.org/Sasja/hsmfui.svg?branch=master)](https://travis-ci.org/Sasja/hsmfui) [![codecov](https://codecov.io/gh/Sasja/hsmfui/branch/master/graph/badge.svg)](https://codecov.io/gh/Sasja/hsmfui)

hierarchical state machine for embedded user interfaces

run ```make test``` to compile and test

